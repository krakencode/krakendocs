---
title: "Document Utils"
subtitle: Some cool tools
date: 2018-12-07T11:27:14-05:00
categories: [Utilities]
tags: [Document, FLowchart, MSC, Graphiz]
draft: false

# Left Bar Options
show_left_bar: true
show_toc: true
show_tags: true
show_categories: true

# Content Options
show_flowchart: true
show_msc: true
show_viz: true
---

# Code Blocks

A Code block allows you to show the code with color highlights. To see a list of supported languages you can visit [here](  https://highlightjs.org/static/demo/).

To show a code block wrap the code with the three ticks on either side. If you wish to highlight the code you must specify the language this is hown in the example

**Exampnle:**

``` java
public class HelloWorld {
   public static void main(String[] args) {
      // Prints "Hello, World" in the terminal window.
      System.out.println("Hello, World");
   }
}
```

**Markdown**
``` markdown
    ``` java
    public class HelloWorld {
        public static void main(String[] args) {
            // Prints "Hello, World" in the terminal window.
            System.out.println("Hello, World");
        }
    }
    ```
```


# Flowcharts

You can also show cool looking flowcharts using flowcharts.js. You must set flowchart to true in the page params and set the code block language to flowchart. For syntax help check [here](http://flowchart.js.org/)

```flowchart
  st=>start: Start:>http://www.google.com[blank]
  e=>end:>http://www.google.com
  op1=>operation: My Operation
  sub1=>subroutine: My Subroutine
  cond=>condition: Yes
  or No?:>http://www.google.com
  io=>inputoutput: catch something...
  para=>parallel: parallel tasks

  st->op1->cond
  cond(yes)->io->e
  cond(no)->para
  para(path1, bottom)->sub1(right)->op1
  para(path2, top)->op1
```

```markdown
    ```flowchart
    st=>start: Start:>http://www.google.com[blank]
    e=>end:>http://www.google.com
    op1=>operation: My Operation
    sub1=>subroutine: My Subroutine
    cond=>condition: Yes
    or No?:>http://www.google.com
    io=>inputoutput: catch something...
    para=>parallel: parallel tasks

    st->op1->cond
    cond(yes)->io->e
    cond(no)->para
    para(path1, bottom)->sub1(right)->op1
    para(path2, top)->op1
    ```
```

# Message Sequence Charts

To simply add a  MSC all you do is add the code in a code block with language of "msc" and then set the page params "msc" to true.

To see the MSC syntax check [here](https://bramp.github.io/js-sequence-diagrams/)

```msc
Andrew->China: Says Hello
Note right of China: China thinks\nabout it
China-->Andrew: How are you?
Andrew->>China: I am good thanks!
```

```markdown
    ```msc
    Andrew->China: Says Hello
    Note right of China: China thinks\nabout it
    China-->Andrew: How are you?
    Andrew->>China: I am good thanks!
    ```
```

# Graphiz

```viz-dot
  digraph g { 
  node[shape="circle" , label="", width=0.2, height=0.2]
  l1[xlabel="v\(s\)"]
  l21[xlabel="a", width=0.1, height=0.1 , style=filled]
  l22[width=0.1, height=0.3, style=filled]
  l31[xlabel="v\(s'\)"]

  l1 -> l21
  l1 -> l22
  l21 -> l31 [xlabel="r"]
  l21 -> l32
  l22 -> l33
  l22 -> l34
  }
```

```markdown
    ```viz-dot
    digraph g { 
    node[shape="circle" , label="", width=0.2, height=0.2]
    l1[xlabel="v\(s\)"]
    l21[xlabel="a", width=0.1, height=0.1 , style=filled]
    l22[width=0.1, height=0.3, style=filled]
    l31[xlabel="v\(s'\)"]

    l1 -> l21
    l1 -> l22
    l21 -> l31 [xlabel="r"]
    l21 -> l32
    l22 -> l33
    l22 -> l34
    }
    ```
```