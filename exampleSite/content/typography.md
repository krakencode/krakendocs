---
title: "Typography"
subtitle:
date: 2018-12-07T11:23:16-05:00
categories: [Typography]
tags: [Text, Images, Code, Formating]
draft: false

# Left Bar Options
show_left_bar: true
show_toc: true
show_tags: true
show_categories: true

# Content Options
show_flowchart: false
show_msc: false
show_viz: false
---


# Headers

We are not showing an example of the header task as it would make the TOC ( over on right ) look crazy so this is what they are and you can see some examples used on this page.

```markdown
# H1
## H2
### H3
#### H4
##### H5
###### H6
```

# Type

## Emphasis ( italics )

There are two ways to show text as italics either with ```*``` or ```_```

Emphasis, aka italics, with *asterisks* or _underscores_.

```markdown
Emphasis, aka italics, with *asterisks* or _underscores_.
```

## Strong ( bold )

Once again there are two ways to show text as bold either with ```**``` or ```__```

Strong emphasis, aka bold, with **asterisks** or __underscores__.

```markdown
Strong emphasis, aka bold, with **asterisks** or __underscores__.
```

## Combined ( italics + bold )

You can combine the two like so.

You just have to combine the two **_Style 1_** or __*Style 2*__

```markdown
You just have to combine the two **_Style 1_** or __*Style 2*__
```

## Strike Through

Cross out a word or sentence

Strikethrough uses two tildes. ~~Scratch this.~~

```markdown
Strikethrough uses two tildes. ~~Scratch this.~~
```

## Blockquotes

For quoting blocks of content from another source within your document. 

> So that it looks like this inside you normal messages
> This line is part of the same quote.

You will want to add a ```>``` at the beginning of each line 

```markdown
> So that it looks like this inside you normal messages
> This line is part of the same quote.
```

# Links

Links you know links here are some basic examples

## Basic

* Basic Link:  [Link Text](https://link/url)
* Link with a title (hover over it): [Link Text](https://link/url "Tooltip Text")
* https://www.google.com is just a link with the url being the text

```markdown
* Basic Link:  [Link Text](https://link/url)
* Link with a title (hover over it): [Link Text](https://link/url "Tooltip Text")
* https://www.google.com is just a link with the url being the text
```

## By Reference

These look just like the ones above but you are using a reference instead 

* Basic Link:  [Link Text][urlLink]
* Link with a title (hover over it): [Link Text][urlLinkWTitle]
* [urlLink] is just a link with the url being the text

[urlLink]: https://wwww.google.com
[urlLinkWTitle]: https://wwww.google.com "ToolTip from Ref"

```markdown
* Basic Link:  [Link Text][urlLink]
* Link with a title (hover over it): [Link Text][urlLinkWTitle]
* [urlLink] is just a link with the url being the text

[urlLink]: https://wwww.google.com
[urlLinkWTitle]: https://wwww.google.com "ToolTip from Ref"
```


# Images

## Inline-style: 

![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

```markdown
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")
```

## Reference-style: 

![alt text][logo]
[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"

```markdown
![alt text][logo]
[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"
```

# Lists

## Ordered Lists

A list with bullets as 1 2 .... 10 the list can also have sub items just by indenting with 2 spaces and then the number. If you have a sub on a sub each indent is 3 spaces. 

**_Note:_** The numbers can all be "1" and the code will auto increment them.

1. Main 1
1. Main 2
  1. Sub 1
     1. Sub Sub 1
  1. Sub 2
1. Main 3

```markdown
1. Main 1
1. Main 2
  1. Sub 1
     1. Sub Sub 1
  1. Sub 2
1. Main 3
```

## Un-Ordered List

A list of items in no particular order. 

* item 1
* item 2
  * sub-item 1
     * Sub sub-item
  * sub-item 2
* item 3

```markdown
* item 1
* item 2
  * sub-item 1
     * Sub sub-item
  * sub-item 2
* item 3
```

# Tables

Look at the column headers as examples on how to align the data

* Left align "---"
* Right align "--:"
* Center align ":--:"

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| col 1 is      | left-aligned  |    $1 |

```markdown
| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| col 1 is      | left-aligned  |    $1 |
```



# Misc

Here is just some other basics with no good place to put them


## Horizontal Rules

You can use three deferent characters to crete a horizontal rule

---

* Hyphens ```---```
* Asterisks ```***```
* Underscores ```___```

```markdown
Hyphens 

---

Asterisks

***

Underscores

___
```