---
title: "Page One"
subtitle:
date: 2018-12-07T11:30:50-05:00
categories: [Example]
tags: [example, page1]
draft: false

# Left Bar Options
show_left_bar: false
show_toc: false
show_tags: true
show_categories: true

# Content Options
show_flowchart: false
show_msc: false
show_viz: false
---

# this is page 1