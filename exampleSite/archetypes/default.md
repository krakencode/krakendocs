---
title: "{{ replace .Name "-" " " | title }}"
subtitle:
date: {{ .Date }}
categories: []
tags: []
draft: false

# Left Bar Options
show_left_bar: false
show_toc: false
show_tags: true
show_categories: true

# Content Options
show_flowchart: false
show_msc: false
show_viz: false
---

