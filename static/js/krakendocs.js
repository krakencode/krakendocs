// ---------------------------------------------------------------------[ TOC ]-
$('#TableOfContents').each(function(){
    $(this).find('a').addClass('text-white')
})

$('#TableOfContents').children().each(function(){
    if($(this).is("ul")) {
        $(this).addClass('list-unstyled')
    } 
})

$( document ).ready(function() {
    $('code.hljs').each(function() {
        var langName = ""
        var classList = $(this).attr('class').split(/\s+/);

        // Loop over all classes looking for language
        $.each(classList, function(index, item) {
            if (item.match("^language-")) {
                langName = item.replace(/^language-/, '');
            }
        });

        // If nothing show unknown
        if (langName == "") {
            langName = "unknown"
        }
        
        // Add Clipboard section
        $(this).addClass("border border-dark");
        var outHtml = `<div class="border border-dark px-1"><small><strong>Language:</strong>` + langName + `</small>`
        outHtml = outHtml + `<a class="float-right text-dark clipboard" href=""><i class="fas fa-copy"></i></a>`
        outHtml = outHtml + `</div>`        
        $(this).after(outHtml)
    })

    $('a.clipboard').click(function(){
        clipboard.writeText($(this).closest('pre').find('code').text());
        return false;
    })

    // Update Blockquotes
    $('blockquote').each(function() {
        $(this).addClass('blockquote')
        $(this).find('p').each(function(){
            $(this).addClass('mb-0')
        })
    })   
});

hljs.initHighlightingOnLoad();
